#!/bin/sh

_go_check()(
    echo "checking Go..."
    go version
    _go_ec=$?
    if [ $_go_ec -eq 0 ]; then
        echo "Go is ready...
"
        return $_go_ec
    fi

    echo "please ready Go.
you can download from https://go.dev/dl/ then choose according to your
machine OS.
you can check installation instruction in https://go.dev/doc/install.
"
    return $_go_ec
)

_go_check