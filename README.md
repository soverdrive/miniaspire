# Mini Aspire
## How to Run
Mini Aspire app can be run just by simply executing `make`. Using makefile,
the dependency will be checked and the app will be build.
```
make
```

Then a database is needed to run the Mini Aspire app. Migrating database tables
and insertin data seed for testing can be done using rule `migrate`.
```
make migrate
```

Lastly, to run the app, rule `run` can be used.
```
make run
```

Mini Aspire app only needs Go to run.

## API List
To sucesfully execute the API, a header must present. Assuming that the request
is already authenticated, header `user_id` contains user UUID is a must.

### Make Loan
For user to request a loan. Repayment and schedule of repayment is calculated
by Mini Aspire app.

### View Loan
User can see the loan that is requested and its status.

### Admin View Loan
An admin user can see all the loan that is requested to Mini Aspire app.

### Admin Approval
An admin user can approve user's loan request.

## Architecture
### Code Layout
Mini Aspire adopts a concept called clean code by separating code to layers.
There're four layers that is used: a Controller, a Repository, a Model layer,
then a Service layer.

To avoid import cyclic through development, some rules has to be obeyed.
Following is the diagram to the code layout and the relation of each layer.

```
                   •••>  Controller Layer
                   •
                   •         ^
                   •         •
                   •         •
   Model Layer •••••••>  Service Layer
                   •
                   •         ^
                   •         •
                   •         •
                   •••>  Repository Layer
```
Rules
- A Controller Layer is the first layer and must not be imported to other layers
- A Service Layer can only be imported by Controller Layer
- A Repository Layer can be imported to the Service Layer
- A Model Layer can be imported to other three layers and must not import any
of the three other Layer.

**Controller Layer**

A Controller layer holds the HTTP handle that is registered to HTTP router path
in the main function.

There should be no business logic in Controller layer, only a validation of
requests that doesn't require business context and constructing data as the
response to HTTP request.

**Repository Layer**

A Repository layer holds the interaction between Go code and external services
like sqlite3 or some HTTP calls.

A Repository layer should only bothers with constructing instruction to the
external services, control how the request will be sent, and parsing the
response of the request.

**Service layer**

A Service layer exists to stitch several Repistories to make a business logic.

A Service layer should return specific error so it can be mapped in Controller
layer with proper HTTP response code or error message.

**Model Layer**

A Model layer stores various structs definition.

A Model layer should not know the context of business logic but can be used to
verify or calculate something stateless related to the business logic, e.g.

a field in struct can only be filled with string `init`, `progress` and `done`.
a string `done` can only be assigned if another field holds value integer of
more than or equal 100.

### Code Vendoring
The dependencies is handled by Go module and all of the dependencies is
vendored and included in codebase.

The vendored dependencies can save time in CI pipeline because it removes the
need to fetch the dependencies everytime the pipeline is triggered. It can also
serve as a lock to store the dependencies version locally without relying on
the internet that may be causing a problem someday if there's an urgent
hotifix that need to deployed immediately.

### Database
This Mini Aspire is using sqlite3 as database.

The porting version in Go (https://github.com/glebarez/go-sqlite) is chosen
instead of original version of sqlite3 (https://www.sqlite.org/index.html)
in consideration of program dependency and security.

Less dependency is better to maximize portability and security is rather
ignored because Mini Aspire is a test program, not something that need to be
deployed to production and accessed by public users.
#### Table Schema