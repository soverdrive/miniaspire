mini_aspire: check
	@ go build

.phony: run
run:
	./mini_aspire

.phony: check
check:
	@ ./check.sh

.phony: migrate
migrate:
	@ go run ./script/migration.go