CREATE TABLE IF NOT EXISTS user (
    id TEXT, -- a UUID identifier
    username TEXT, -- a username prefered by user
    pass TEXT, -- a hashed password for authentication
    level TEXT, -- a user level, value: user | admin
    created_at TEXT, -- a timestamp with timezone
    updated_at TEXT -- a timestamp with timezone
);

CREATE TABLE IF NOT EXISTS loan (
    id TEXT, -- a UUID identifier
    user_id TEXT, -- a reference to table user.id
    term INTEGER, -- the term agreed by user
    amount TEXT, -- some amount requested by user
    currency TEXT, -- the currency requested by user
    status TEXT, -- the status of loan, value: PENDING | APPROVED | PAID
    requested_at TEXT, -- a timestamp with timezone when user request a loan
    overflow_amount TEXT, -- some amount if user paid more than requested
    settled_at TEXT, -- a timestamp with timezone when user PAID the loan
    created_at TEXT, -- a timestamp with timezone
    updated_at TEXT -- a timestamp with timezone
);

CREATE TABLE IF NOT EXISTS loan_repayment (
    id TEXT, -- a UUID identifier
    loan_id TEXT, -- a reference to table loan.id
    amount TEXT, -- some amount that user need to pay
    schedule TEXT, -- the latest timestamp with timezone when user has to pay
    paid_amount TEXT, -- some amount that user pay for this schedule
    status TEXT, -- the status of repayment, value: PAID | OVER | LESS
    created_at TEXT, -- a timestamp with timezone
    updated_at TEXT -- a timestamp with timezone
);

CREATE TABLE IF NOT EXISTS admin_trail (
    id TEXT, -- a UUID identifier
    admin_user_id TEXT, -- a referenee to table user.id
    loan_id TEXT, -- a reference to table loan.id
    approved_at TEXT, -- a timestamp with timezone when admin approve the loan
    created_at TEXT -- a timestamp with timezone
);