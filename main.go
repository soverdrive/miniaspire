package main

import (
	"database/sql"
	"fmt"
	"net/http"
	"os"

	_ "github.com/glebarez/go-sqlite"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/soverdrive/mini_aspire/internal/controller"
	"gitlab.com/soverdrive/mini_aspire/internal/lib"
	"gitlab.com/soverdrive/mini_aspire/internal/repository"
	"gitlab.com/soverdrive/mini_aspire/internal/service"
)

func main() {
	fmt.Fprintf(os.Stdout, "this is a mini-aspire app in Go.\n")

	// connect
	db, err := sql.Open("sqlite", "./mini_aspire.db")
	if err != nil {
		fmt.Fprintf(os.Stderr, "opening sqlite %v\n", err)
		os.Exit(1)
	}

	miniAspireRouter := chi.NewRouter()
	repoUser := repository.NewUser(db, db)
	ctrlMiddleware := controller.NewMiddleware(repoUser)

	miniAspireRouter.Use(
		ctrlMiddleware.LogMiddleware,
		middleware.Recoverer,
		ctrlMiddleware.UserMiddleware,
	)

	timeMaker := lib.NewTimeMaker()
	uuidMaker := lib.NewUUIDV1Maker()
	repoLoan := repository.NewLoan(db, db)
	svcLoan := service.NewLoan(timeMaker, uuidMaker, repoLoan)
	ctrlLoan := controller.NewLoan(svcLoan)

	miniAspireRouter.Post("/loan", ctrlLoan.LoanRequest)
	miniAspireRouter.Get("/loan", ctrlLoan.ViewLoan)
	miniAspireRouter.Post("/loan/pay", ctrlLoan.PayRepayment)

	adminAspireRouter := miniAspireRouter.With(ctrlMiddleware.AdminMiddleware)
	adminAspireRouter.Get("/admin/loan", ctrlLoan.AdminView)
	adminAspireRouter.Post("/admin/loan/approval", ctrlLoan.AdminApproval)

	miniAspireRouter.Get("/ping", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("pong"))
	})

	// get SQLite version
	var sqlLiteVersion string
	err = db.QueryRow("select sqlite_version()").Scan(&sqlLiteVersion)
	if err != nil {
		fmt.Fprintf(os.Stderr, "checking sqlite version %v\n", err)
		os.Exit(1)
	}

	fmt.Fprintf(os.Stdout, "sqlite version is %s\n", sqlLiteVersion)

	fmt.Fprintf(os.Stdout, "start serving on :8000...\n")
	http.ListenAndServe(":8000", miniAspireRouter)
}
