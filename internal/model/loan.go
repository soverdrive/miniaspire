package model

import (
	"context"
	"database/sql"
	"fmt"
	"math/big"
	"strconv"
	"time"

	"gitlab.com/soverdrive/mini_aspire/internal/lib"
)

type LoanRequestRequest struct {
	Amount            string                 `json:"amount"`
	AmountFloat       *big.Float             `json:"-"`
	Currency          string                 `json:"currency"`
	Term              int64                  `json:"term"`
	Repayment         []LoanRequestRepayment `json:"-"`
	UserID            string                 `json:"-"`
	RequestedAtString string                 `json:"requested_at"`
	RequestedAt       time.Time              `json:"-"`
}

type LoanRequestRepayment struct {
	ScheduleString string
	AmountString   string
	Schedule       time.Time
}

func (lrrepay LoanRequestRepayment) ConstructRepaymenRepoParam(id string) RepaymentRepoParam {
	return RepaymentRepoParam{
		ID:             id,
		ScheduleString: lrrepay.ScheduleString,
		AmountString:   lrrepay.AmountString,
	}
}

var ZeroFloat = big.NewFloat(0.00)

func (lrr *LoanRequestRequest) Validate(ctx context.Context) error {
	// must convertible to float

	tempAmount, err := strconv.ParseFloat(lrr.Amount, 64)
	if err != nil {
		return fmt.Errorf("must be number")
	}

	// going extra mile by using big number package to ensure correct parsing
	// from string to float64.
	amountFloat := big.NewFloat(tempAmount)

	if amountFloat.Cmp(ZeroFloat) <= 0 {
		return fmt.Errorf("must be more than zero")
	}

	lrr.AmountFloat = amountFloat

	switch lrr.Currency {
	case "SGD", "USD", "IDR":
	default:
		return fmt.Errorf("only receive SGD, USD, or IDR")
	}

	if lrr.Term < 1 {
		return fmt.Errorf("term must be 1 at minimum")
	}

	if lrr.RequestedAtString == "" {
		return fmt.Errorf("timestamp for loan request is missing")
	}

	lrr.RequestedAt, err = time.Parse("2006-01-02 15:05:06Z07:00", lrr.RequestedAtString)
	if err != nil {
		lib.LogData(ctx, fmt.Sprintf("valid format is %s", "2006-01-02 15:05:06Z07:00"))
		return fmt.Errorf("invalid timestamp format")
	}

	return nil
}

func (lrr *LoanRequestRequest) CalculateRepayment(ctx context.Context, precision int) []LoanRequestRepayment {
	// float rounding template according to precision param.
	roundingTemplate := fmt.Sprintf("%%.%df", precision)

	// try to divide the amount requested by number of term.
	amountFloat, _ := lrr.AmountFloat.Float64()
	repaymentAmount := amountFloat / float64(lrr.Term)

	// calculate until max length - 1. the last index is reserved for
	// commutative calculation to ensure nice and rounding number.
	repayment := make([]LoanRequestRepayment, lrr.Term)
	nextRepaymentDate := lrr.RequestedAt
	var accumulation float64
	for i := int64(0); i < lrr.Term-1; i++ {
		// parse to string first to round to desired precision then convert
		// them back to float64 to remove hidden decimal and mantisa.
		repayment[i].AmountString = fmt.Sprintf("%s %s", lrr.Currency,
			fmt.Sprintf(roundingTemplate, repaymentAmount))

		nextRepaymentDate = nextRepaymentDate.AddDate(0, 0, 7)
		repayment[i].Schedule = nextRepaymentDate
		repayment[i].ScheduleString = repayment[i].Schedule.Format("2006-01-02 15:05:06Z07:00")

		// parse to string first to round to desired precision then convert
		// them back to float64 to remove hidden decimal and mantisa.
		repaymentSimplified, _ := strconv.ParseFloat(fmt.Sprintf("%.2f", repaymentAmount), 64)
		accumulation += repaymentSimplified
	}

	// to ensure the last repayment added nicely to loan amount, substract
	// loan amount to accumulation.
	// parse to string first to round to desired precision then convert
	// them back to float64 to remove hidden decimal and mantisa.
	accumumlationSimplified, _ := strconv.ParseFloat(fmt.Sprintf("%.2f", accumulation), 64)

	lastIdx := lrr.Term - 1
	repayment[lastIdx].AmountString = fmt.Sprintf("%s %s", lrr.Currency,
		fmt.Sprintf(roundingTemplate, amountFloat-accumumlationSimplified))
	repayment[lastIdx].Schedule = nextRepaymentDate.AddDate(0, 0, 7)
	repayment[lastIdx].ScheduleString = repayment[lastIdx].Schedule.Format("2006-01-02 15:05:06Z07:00")

	return repayment
}

type LoanRequestResponse struct {
	Result string `json:"result"`
}

type LoanRequestRepoParam struct {
	ID                string
	Amount            string
	Currency          string
	Term              int64
	UserID            string
	RequestedAtString string
	Status            string
	CreatedAt         string
	UpdatedAt         string
}

type RepaymentRepoParam struct {
	ID             string
	ScheduleString string
	AmountString   string
	CreatedAt      string
	UpdatedAt      string
}

type ViewLoanRequest struct {
	UserID string
}

type LoanRepayment struct {
	Amount     string `json:"amount"`
	Schedule   string `json:"schedule"`
	PaidAmount string `json:"paid_amount"`
	Status     string `json:"status"`
}

type Loan struct {
	Amount      string          `json:"amount"`
	Term        int64           `json:"term"`
	RequestedAt string          `json:"requested_at"`
	Repayment   []LoanRepayment `json:"repayment"`
	Status      string          `json:"status"`
	Currency    string          `json:"currency"`
}

type AdminLoan struct {
	Loan
	UserID string `json:"user_id"`
	ID     string `json:"id"`
}

type ViewLoanRepaymentData struct {
	Amount     string
	Schedule   string
	PaidAmount sql.NullString
	Status     sql.NullString
}

type ViewLoanData struct {
	Amount      string
	Term        int64
	Currency    string
	RequestedAt string
	Repayment   []ViewLoanRepaymentData
	Status      sql.NullString
}

type AdminViewLoanRepaymentData struct {
	ID         string
	Amount     string
	Schedule   string
	PaidAmount sql.NullString
	Status     sql.NullString
}

type AdminViewLoanData struct {
	Amount      string
	Term        int64
	Currency    string
	RequestedAt string
	Repayment   []AdminViewLoanRepaymentData
	Status      sql.NullString
	UserID      string
	ID          string
}

type repaymentProgressCheck struct {
	NextLoanStatus         string
	RepaymentID            string
	CurrentRepaymentStatus string
}

var ErrRepaymentWantToPaid = fmt.Errorf("repayment amount is less than it should be")

func (vld *AdminViewLoanData) RepaymentProgressCheck(ctx context.Context, paidAmount string) (repaymentProgressCheck, error) {
	// check which repayment is going to be paid
	var chosenRepayment AdminViewLoanRepaymentData
	var totalPaid float64
	var chosenIdx int
	for idx, oneRepayment := range vld.Repayment {
		// if not status registered and paid amount is null, then choose as
		// repayment to be paid.
		// calculate the rest of the amount to determine whether user is
		// overpaid, underpaid, or paid the exact same amount.
		if !oneRepayment.Status.Valid && !oneRepayment.PaidAmount.Valid {
			chosenIdx = idx
			chosenRepayment = vld.Repayment[idx]
			break
		}

		currentPaid, err := strconv.ParseFloat(oneRepayment.PaidAmount.String, 64)
		if err != nil {
			return repaymentProgressCheck{}, err
		}

		totalPaid += currentPaid
	}

	totalLoan, err := strconv.ParseFloat(vld.Amount, 64)
	if err != nil {
		return repaymentProgressCheck{}, err
	}

	wantToPaidAmount, err := strconv.ParseFloat(paidAmount, 64)
	if err != nil {
		return repaymentProgressCheck{}, err
	}

	needToPayAmount, err := strconv.ParseFloat(chosenRepayment.Amount, 64)
	if err != nil {
		return repaymentProgressCheck{}, err
	}

	if wantToPaidAmount < needToPayAmount {
		return repaymentProgressCheck{}, ErrRepaymentWantToPaid
	}

	diffAmount := totalLoan - (totalPaid + wantToPaidAmount)
	lastRepayment := chosenIdx == len(vld.Repayment)-1
	// special case, if it's last repayment and there's more than zero
	// difference, then the user has not paid the whole loan.
	if lastRepayment && diffAmount > 0 {
		return repaymentProgressCheck{}, ErrRepaymentWantToPaid
	}

	var loanVerdict string
	if lastRepayment && diffAmount >= 0 {
		loanVerdict = "PAID"
	}

	return repaymentProgressCheck{
		NextLoanStatus:         loanVerdict,
		RepaymentID:            chosenRepayment.ID,
		CurrentRepaymentStatus: "PAID",
	}, nil
}

type PayRepaymentRequest struct {
	LoanID string `json:"loan_id"`
	Amount string `json:"amount"`
}

type PayRepaymentParam struct {
	LoanID string
	Amount string
	UserID string
}

type PayRepaymentRepoParam struct {
	LoanID      string
	RepaymentID string
	Amount      string
	Status      string
}

type PayRepaymentResponse struct {
	Status string `json:"status"`
}
