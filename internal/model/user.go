package model

type User struct {
	ID        string
	Username  string
	Level     string
	CreatedAt string
	UpdatedAt string
}
