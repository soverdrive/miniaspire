package model

type AdminApprovalRequest struct {
	AdminUserID string `json:"-"`
	LoanID      string `json:"loan_id"`
}

type AdminApprovalParam struct {
	AdminTrailID string
	AdminUserID  string
	LoanID       string
	ApprovedAt   string
	CreatedAt    string
}

type AdminApprovalResponse struct {
	Status string `json:"status"`
}
