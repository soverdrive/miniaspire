package repository

import (
	"context"
	"database/sql"
	"fmt"
	"strings"

	"gitlab.com/soverdrive/mini_aspire/internal/lib"
	"gitlab.com/soverdrive/mini_aspire/internal/model"
)

type LoanRepository struct {
	writeDB *sql.DB
	readDB  *sql.DB
}

func NewLoan(writeDB, readDB *sql.DB) *LoanRepository {
	return &LoanRepository{
		writeDB: writeDB,
		readDB:  readDB,
	}
}

func (lr *LoanRepository) BeginTx(ctx context.Context) (*sql.Tx, error) {
	return lr.writeDB.Begin()
}

func (lr *LoanRepository) InsertLoan(ctx context.Context, dbTx *sql.Tx, param model.LoanRequestRepoParam) (string, error) {
	res := dbTx.QueryRowContext(ctx, `INSERT INTO loan (id, user_id, term, amount, 
currency, status, requested_at, created_at, updated_at)
VALUES ($1, $2, $3, $4,
$5, $6, $7, $8, $9) RETURNING id`,
		param.ID,
		param.UserID,
		param.Term,
		param.Amount,
		param.Currency,
		param.Status,
		param.RequestedAtString,
		param.CreatedAt,
		param.UpdatedAt,
	)

	var newID string
	return newID, res.Scan(&newID)
}

func (lr *LoanRepository) InsertRepayment(ctx context.Context, dbTx *sql.Tx, loanID string, param []model.RepaymentRepoParam) error {
	// id, loan_id, amount, schedule, created_at, updated_at
	insertTemplate := fmt.Sprintf("($%%d, $%%d, $%%d, $%%d, $%%d, $%%d)")

	// build bulk insert values
	var insertBulk []string
	var argBulk []interface{}
	// number of args each row from param
	var multiplier = 6
	for idx, oneParam := range param {
		currentBase := idx * multiplier
		insertBulk = append(insertBulk, fmt.Sprintf(insertTemplate,
			currentBase+1,
			currentBase+2,
			currentBase+3,
			currentBase+4,
			currentBase+5,
			currentBase+6,
		))
		argBulk = append(argBulk, oneParam.ID, loanID, oneParam.AmountString, oneParam.ScheduleString, oneParam.CreatedAt, oneParam.UpdatedAt)
	}

	queryInsertRepayment := fmt.Sprintf("INSERT INTO loan_repayment (id, loan_id, amount, schedule, created_at, updated_at) VALUES %s",
		strings.Join(insertBulk, ", "))

	_, err := dbTx.ExecContext(ctx, queryInsertRepayment, argBulk...)
	return err
}

func (lr *LoanRepository) ViewLoan(ctx context.Context, userID string) ([]model.ViewLoanData, error) {
	res, err := lr.readDB.QueryContext(ctx, `SELECT l.id, l.term, l.amount, l.currency, l.status, l.requested_at,
lr.amount, lr.schedule, lr.status, lr.paid_amount FROM loan AS l INNER JOIN loan_repayment AS lr ON l.id = lr.loan_id
WHERE l.user_id = $1 ORDER BY l.id, l.requested_at, lr.created_at DESC`, userID)
	if err != nil {
		return []model.ViewLoanData{}, err
	}

	// to keep the order of loan
	var loanSeries []string
	// map of loan.id and loan data
	var loanData = make(map[string]model.ViewLoanData)
	// map of loan.id and loan repayment data
	var repaymentData = make(map[string][]model.ViewLoanRepaymentData)
	for res.Next() {
		var loanID string
		var oneLoan model.ViewLoanData
		var oneRepayment model.ViewLoanRepaymentData
		err := res.Scan(
			&loanID,
			&oneLoan.Term,
			&oneLoan.Amount,
			&oneLoan.Currency,
			&oneLoan.Status,
			&oneLoan.RequestedAt,
			&oneRepayment.Amount,
			&oneRepayment.Schedule,
			&oneRepayment.Status,
			&oneRepayment.PaidAmount,
		)
		if err != nil {
			lib.LogData(ctx, err)
			return []model.ViewLoanData{}, err
		}

		if _, ok := loanData[loanID]; !ok {
			loanSeries = append(loanSeries, loanID)
		}

		loanData[loanID] = oneLoan
		repaymentData[loanID] = append(repaymentData[loanID], oneRepayment)
	}

	if len(loanSeries) == 0 {
		return []model.ViewLoanData{}, sql.ErrNoRows
	}

	var finalLoanData []model.ViewLoanData
	// construct loan data
	for _, oneLoanID := range loanSeries {
		loanDataRes := loanData[oneLoanID]
		loanDataRes.Repayment = repaymentData[oneLoanID]

		finalLoanData = append(finalLoanData, loanDataRes)
	}

	return finalLoanData, nil
}

func (lr *LoanRepository) AdminViewLoan(ctx context.Context) ([]model.AdminViewLoanData, error) {
	res, err := lr.readDB.QueryContext(ctx, `SELECT l.id, l.user_id, l.term, l.amount, l.currency, l.status, l.requested_at,
lr.amount, lr.schedule, lr.status, lr.paid_amount, lr.id FROM loan AS l INNER JOIN loan_repayment AS lr ON l.id = lr.loan_id
WHERE l.status = 'PENDING' ORDER BY l.id, l.requested_at, lr.created_at DESC`)
	if err != nil {
		return []model.AdminViewLoanData{}, err
	}

	return lr.getLoan(ctx, res)
}

func (lr *LoanRepository) getLoan(ctx context.Context, res *sql.Rows) ([]model.AdminViewLoanData, error) {
	// to keep the order of loan
	var loanSeries []string
	// map of loan.id and loan data
	var loanData = make(map[string]model.AdminViewLoanData)
	// map of loan.id and loan repayment data
	var repaymentData = make(map[string][]model.AdminViewLoanRepaymentData)
	for res.Next() {
		var oneLoan model.AdminViewLoanData
		var oneRepayment model.AdminViewLoanRepaymentData
		err := res.Scan(
			&oneLoan.ID,
			&oneLoan.UserID,
			&oneLoan.Term,
			&oneLoan.Amount,
			&oneLoan.Currency,
			&oneLoan.Status,
			&oneLoan.RequestedAt,
			&oneRepayment.Amount,
			&oneRepayment.Schedule,
			&oneRepayment.Status,
			&oneRepayment.PaidAmount,
			&oneRepayment.ID,
		)
		if err != nil {
			lib.LogData(ctx, err)
			return []model.AdminViewLoanData{}, err
		}

		if _, ok := loanData[oneLoan.ID]; !ok {
			loanSeries = append(loanSeries, oneLoan.ID)
		}

		loanData[oneLoan.ID] = oneLoan
		repaymentData[oneLoan.ID] = append(repaymentData[oneLoan.ID], oneRepayment)
	}

	if len(loanSeries) == 0 {
		return []model.AdminViewLoanData{}, sql.ErrNoRows
	}

	var finalLoanData []model.AdminViewLoanData
	// construct loan data
	for _, oneLoanID := range loanSeries {
		loanDataRes := loanData[oneLoanID]
		loanDataRes.Repayment = repaymentData[oneLoanID]

		finalLoanData = append(finalLoanData, loanDataRes)
	}

	return finalLoanData, nil
}

func (lr *LoanRepository) CheckLoanByID(ctx context.Context, dbTx *sql.Tx, loanID, status string) (string, error) {
	// could use SELECT FOR UPDATE clause if it's on MySQL or Postgres to lock
	// the row.
	row := dbTx.QueryRowContext(ctx, `SELECT id FROM loan WHERE id = $1 AND status = $2`, loanID, status)

	var id string
	return id, row.Scan(&id)
}

func (lr *LoanRepository) AdminApproval(ctx context.Context, dbTx *sql.Tx, param model.AdminApprovalParam) (string, error) {
	_, err := dbTx.ExecContext(ctx, `INSERT INTO admin_trail (id, admin_user_id, loan_id, approved_at, created_at)
VALUES ($1, $2, $3, $4, $5)`,
		param.AdminTrailID,
		param.AdminUserID,
		param.LoanID,
		param.ApprovedAt,
		param.CreatedAt)
	if err != nil {
		return "", err
	}

	return "APPROVED", err
}

func (lr *LoanRepository) ApproveALoan(ctx context.Context, dbTx *sql.Tx, loanID string) error {
	_, err := dbTx.ExecContext(ctx, `UPDATE loan SET status = 'APPROVED' WHERE id = $1`, loanID)

	return err
}

func (lr *LoanRepository) GetLoanByID(ctx context.Context, dbTx *sql.Tx, userID, loanID string) (model.AdminViewLoanData, error) {
	res, err := dbTx.QueryContext(ctx, `SELECT l.id, l.term, l.amount, l.currency, l.status, l.requested_at,
lr.amount, lr.schedule, lr.status, lr.paid_amount FROM loan AS l INNER JOIN loan_repayment AS lr ON l.id = lr.loan_id
WHERE l.user_id = $1 AND l.id = $2 ORDER BY lr.created_at DESC`, userID, loanID)
	if err != nil {
		return model.AdminViewLoanData{}, err
	}

	finalDataSlice, err := lr.getLoan(ctx, res)
	if err != nil {
		return model.AdminViewLoanData{}, err
	}

	return finalDataSlice[0], nil
}

func (lr *LoanRepository) PayRepayment(ctx context.Context, dbTx *sql.Tx, param model.PayRepaymentRepoParam) error {
	_, err := dbTx.ExecContext(ctx, `UPDATE loan_repayment SET paid_amount = $3, status = $4 WHERE repayment_id = $1, loan_id = $2`,
		param.RepaymentID, param.LoanID, param.Amount, param.Status)

	return err
}

func (lr *LoanRepository) FinishedALoan(ctx context.Context, dbTx *sql.Tx, loanID string) error {
	_, err := dbTx.ExecContext(ctx, `UPDATE loan SET status = 'PAID' WHERE id = $1`, loanID)

	return err
}
