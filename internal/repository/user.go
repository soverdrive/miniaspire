package repository

import (
	"context"
	"database/sql"

	"gitlab.com/soverdrive/mini_aspire/internal/model"
)

type UserRepository struct {
	writeDB *sql.DB
	readDB  *sql.DB
}

func NewUser(writeDB, readDB *sql.DB) *UserRepository {
	return &UserRepository{
		writeDB: writeDB,
		readDB:  readDB,
	}
}

func (ur *UserRepository) GetUserByID(ctx context.Context, userID string) (model.User, error) {
	res := ur.readDB.QueryRowContext(ctx, `SELECT id, username, level, created_at, updated_at
FROM user WHERE id = $1`, userID)

	var userData model.User
	return userData, res.Scan(
		&userData.ID,
		&userData.Username,
		&userData.Level,
		&userData.CreatedAt,
		&userData.UpdatedAt,
	)
}
