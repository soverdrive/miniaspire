package controller

import (
	"context"
	"encoding/json"
	"fmt"
)

type okJSON struct {
	Data     interface{} `json:"data"`
	Metadata interface{} `json:"meta"`
}

func okJSONResponse(ctx context.Context, okMessage interface{}) []byte {
	var okResp okJSON
	okResp.Data = okMessage
	okResp.Metadata = map[string]interface{}{
		"request_id": ctx.Value("log"),
	}

	resp, err := json.Marshal(okResp)
	if err != nil {
		return []byte(fmt.Sprintf("failed to marshal ok response %v", err))
	}

	return resp
}

type badJSON struct {
	Errors   interface{} `json:"errors"`
	Metadata interface{} `json:"meta"`
}

func badJSONResponse(ctx context.Context, errMessage interface{}) []byte {
	var badResp badJSON
	badResp.Errors = map[string]interface{}{
		"error": errMessage,
	}
	badResp.Metadata = map[string]interface{}{
		"request_id": ctx.Value("log"),
	}

	resp, err := json.Marshal(badResp)
	if err != nil {
		return []byte(fmt.Sprintf("failed to marshal bad response %v", err))
	}

	return resp
}
