package controller

import (
	"context"
	"database/sql"
	"fmt"
	"net/http"
	"time"

	"github.com/google/uuid"
	"gitlab.com/soverdrive/mini_aspire/internal/lib"
	"gitlab.com/soverdrive/mini_aspire/internal/model"
)

type UserRepository interface {
	GetUserByID(ctx context.Context, userID string) (model.User, error)
}

type MiddlewareController struct {
	userRepo UserRepository
}

func NewMiddleware(userRepo UserRepository) *MiddlewareController {
	return &MiddlewareController{
		userRepo: userRepo,
	}
}

func (mctrl *MiddlewareController) UserMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		userIDString := r.Header.Get("user_id")
		_, err := uuid.Parse(userIDString)
		if err != nil {
			lib.LogData(r.Context(), fmt.Sprintf("not valid user id %s", userIDString))
			w.WriteHeader(http.StatusUnauthorized)
			w.Write(badJSONResponse(r.Context(), "not authorized"))
			return
		}

		userData, err := mctrl.userRepo.GetUserByID(r.Context(), userIDString)
		if err != nil && err == sql.ErrNoRows {
			w.WriteHeader(http.StatusUnauthorized)
			w.Write(badJSONResponse(r.Context(), "not authorized"))
			return
		} else if err != nil {
			lib.LogData(r.Context(), fmt.Sprintf("err when get user info %v", err))
			w.WriteHeader(http.StatusInternalServerError)
			w.Write(badJSONResponse(r.Context(), "please try again later"))
			return
		}

		userCtx := context.WithValue(r.Context(), "user", userData.ID)
		levelCtx := context.WithValue(userCtx, "user_level", userData.Level)
		newReq := r.WithContext(levelCtx)

		next.ServeHTTP(w, newReq)
	})
}

func (mctrl *MiddlewareController) AdminMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		userLevel, ok := r.Context().Value("user_level").(string)
		if !ok || userLevel != "admin" {
			w.WriteHeader(http.StatusUnauthorized)
			w.Write(badJSONResponse(r.Context(), "not authorized"))
			return
		}

		next.ServeHTTP(w, r)
	})
}

func (mctrl *MiddlewareController) LogMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logID, _ := uuid.NewUUID()
		logCtx := context.WithValue(r.Context(), "log", logID)

		timeCtx := context.WithValue(logCtx, "time", time.Now())

		newReq := r.WithContext(timeCtx)

		next.ServeHTTP(w, newReq)
	})
}
