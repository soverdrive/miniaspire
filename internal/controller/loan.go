package controller

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/soverdrive/mini_aspire/internal/lib"
	"gitlab.com/soverdrive/mini_aspire/internal/model"
)

type loanService interface {
	MakeLoan(ctx context.Context, param model.LoanRequestRequest) (string, error)
	ViewLoan(ctx context.Context, param model.ViewLoanRequest) ([]model.Loan, error)
	AdminView(ctx context.Context) ([]model.AdminLoan, error)
	AdminApproval(ctx context.Context, param model.AdminApprovalRequest) (string, error)
	PayRepayment(ctx context.Context, param model.PayRepaymentParam) (string, error)
}

type LoanController struct {
	loanSvc loanService
}

func NewLoan(loanSvc loanService) *LoanController {
	return &LoanController{
		loanSvc: loanSvc,
	}
}

func (lctrl *LoanController) LoanRequest(w http.ResponseWriter, r *http.Request) {
	payload, err := ioutil.ReadAll(r.Body)
	if err != nil {
		lib.LogData(r.Context(), err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write(badJSONResponse(r.Context(), fmt.Errorf("malicious form of request")))
		return
	}

	var reqPayload model.LoanRequestRequest
	if err := json.Unmarshal(payload, &reqPayload); err != nil {
		lib.LogData(r.Context(), err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write(badJSONResponse(r.Context(), fmt.Errorf("invalid payload")))
		return
	}

	if err := reqPayload.Validate(r.Context()); err != nil {
		lib.LogData(r.Context(), err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write(badJSONResponse(r.Context(), err.Error()))
		return
	}

	ctx := r.Context()
	reqPayload.UserID = ctx.Value("user").(string)

	loanResult, err := lctrl.loanSvc.MakeLoan(ctx, reqPayload)
	if err != nil {
		lib.LogData(ctx, err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(badJSONResponse(ctx, fmt.Errorf("there is something wrong with the process, try again later")))
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write(okJSONResponse(ctx, model.LoanRequestResponse{
		Result: loanResult,
	}))
}

func (lctrl *LoanController) ViewLoan(w http.ResponseWriter, r *http.Request) {
	var viewParam model.ViewLoanRequest
	ctx := r.Context()
	viewParam.UserID = ctx.Value("user").(string)

	loanList, err := lctrl.loanSvc.ViewLoan(ctx, viewParam)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		lib.LogData(ctx, err)
		w.WriteHeader(http.StatusNotFound)
		w.Write(badJSONResponse(ctx, fmt.Errorf("no user found")))
		return
	} else if err != nil {
		lib.LogData(ctx, err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(badJSONResponse(ctx, fmt.Errorf("there is something wrong with the process, try again later")))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(okJSONResponse(ctx, loanList))
}

func (lctrl *LoanController) AdminView(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	loanList, err := lctrl.loanSvc.AdminView(ctx)
	if err != nil {
		lib.LogData(ctx, err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(badJSONResponse(ctx, fmt.Errorf("there is something wrong with the process, try again later")))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(okJSONResponse(ctx, loanList))
}

func (lctrl *LoanController) AdminApproval(w http.ResponseWriter, r *http.Request) {
	payload, err := ioutil.ReadAll(r.Body)
	if err != nil {
		lib.LogData(r.Context(), err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write(badJSONResponse(r.Context(), fmt.Errorf("malicious form of request")))
		return
	}

	var reqPayload model.AdminApprovalRequest
	if err := json.Unmarshal(payload, &reqPayload); err != nil {
		lib.LogData(r.Context(), err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write(badJSONResponse(r.Context(), fmt.Errorf("invalid payload")))
		return
	}

	ctx := r.Context()
	adminUserID := ctx.Value("user").(string)

	reqPayload.AdminUserID = adminUserID

	res, err := lctrl.loanSvc.AdminApproval(ctx, reqPayload)
	if err != nil {
		lib.LogData(ctx, err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(badJSONResponse(ctx, fmt.Errorf("there is something wrong with the process, try again later")))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(okJSONResponse(ctx, model.AdminApprovalResponse{
		Status: res,
	}))
}

func (lctrl *LoanController) PayRepayment(w http.ResponseWriter, r *http.Request) {
	payload, err := ioutil.ReadAll(r.Body)
	if err != nil {
		lib.LogData(r.Context(), err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write(badJSONResponse(r.Context(), fmt.Errorf("malicious form of request")))
		return
	}

	var reqPayload model.PayRepaymentRequest
	if err := json.Unmarshal(payload, &reqPayload); err != nil {
		lib.LogData(r.Context(), err)
		w.WriteHeader(http.StatusBadRequest)
		w.Write(badJSONResponse(r.Context(), fmt.Errorf("invalid payload")))
		return
	}

	ctx := r.Context()
	userID := ctx.Value("user").(string)

	status, err := lctrl.loanSvc.PayRepayment(ctx, model.PayRepaymentParam{
		LoanID: reqPayload.LoanID,
		Amount: reqPayload.Amount,
		UserID: userID,
	})
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		lib.LogData(r.Context(), err)
		w.WriteHeader(http.StatusNotFound)
		w.Write(badJSONResponse(r.Context(), fmt.Errorf("cannot find loan")))
		return
	} else if err != nil && errors.Is(err, model.ErrRepaymentWantToPaid) {
		lib.LogData(r.Context(), err)
		w.WriteHeader(http.StatusNotFound)
		w.Write(badJSONResponse(r.Context(), err))
		return
	} else if err != nil {
		lib.LogData(r.Context(), err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(badJSONResponse(ctx, fmt.Errorf("there is something wrong with the process, try again later")))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(okJSONResponse(ctx, model.PayRepaymentResponse{
		Status: status,
	}))
}
