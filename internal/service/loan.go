package service

import (
	"context"
	"database/sql"
	"fmt"

	"gitlab.com/soverdrive/mini_aspire/internal/lib"
	"gitlab.com/soverdrive/mini_aspire/internal/model"
)

//go:generate mockgen -source=loan.go -package service -destination=loan_mock.go
type UUIDMaker interface {
	UUIDV1() (string, error)
}

type TimeMaker interface {
	Now() string
}

type LoanRepository interface {
	BeginTx(ctx context.Context) (*sql.Tx, error)
	InsertLoan(ctx context.Context, dbTx *sql.Tx, param model.LoanRequestRepoParam) (string, error)
	InsertRepayment(ctx context.Context, dbTx *sql.Tx, loanID string, param []model.RepaymentRepoParam) error
	ViewLoan(ctx context.Context, userID string) ([]model.ViewLoanData, error)
	AdminViewLoan(ctx context.Context) ([]model.AdminViewLoanData, error)
	CheckLoanByID(ctx context.Context, dbTx *sql.Tx, loanID, status string) (string, error)
	AdminApproval(ctx context.Context, dbTx *sql.Tx, param model.AdminApprovalParam) (string, error)
	ApproveALoan(ctx context.Context, dbTx *sql.Tx, loanID string) error
	GetLoanByID(ctx context.Context, dbTx *sql.Tx, userID, loanID string) (model.AdminViewLoanData, error)
	PayRepayment(ctx context.Context, dbTx *sql.Tx, param model.PayRepaymentRepoParam) error
	FinishedALoan(ctx context.Context, dbTx *sql.Tx, loanID string) error
}

type LoanService struct {
	timeMaker TimeMaker
	uuidMaker UUIDMaker
	loanRepo  LoanRepository
}

func NewLoan(timeMaker TimeMaker, uuidMaker UUIDMaker, loanRepo LoanRepository) *LoanService {
	return &LoanService{
		timeMaker: timeMaker,
		uuidMaker: uuidMaker,
		loanRepo:  loanRepo,
	}
}

func (lsvc *LoanService) MakeLoan(ctx context.Context, param model.LoanRequestRequest) (string, error) {
	repayment := param.CalculateRepayment(ctx, 2)
	// lib.LogData(ctx, repayment)

	dbTx, err := lsvc.loanRepo.BeginTx(ctx)
	if err != nil {
		return "", fmt.Errorf("begin %w", err)
	}

	defer func() {
		errRollback := dbTx.Rollback()
		if errRollback != nil && errRollback != sql.ErrTxDone {
			lib.LogData(ctx, errRollback)
		}
	}()

	loanID, err := lsvc.uuidMaker.UUIDV1()
	if err != nil {
		return "", fmt.Errorf("uuid loan %w", err)
	}

	confirmedLoanID, err := lsvc.loanRepo.InsertLoan(ctx, dbTx, model.LoanRequestRepoParam{
		ID:                loanID,
		Amount:            param.Amount,
		Currency:          param.Currency,
		RequestedAtString: param.RequestedAtString,
		Term:              param.Term,
		UserID:            param.UserID,
		Status:            "PENDING",
		CreatedAt:         lsvc.timeMaker.Now(),
		UpdatedAt:         lsvc.timeMaker.Now(),
	})
	if err != nil {
		return "", fmt.Errorf("insert loan %w", err)
	}

	var repaymentRepoParam []model.RepaymentRepoParam
	for _, oneRepayment := range repayment {
		repaymentID, err := lsvc.uuidMaker.UUIDV1()
		if err != nil {
			return "", fmt.Errorf("uuid repayment %w", err)
		}

		repayParam := oneRepayment.ConstructRepaymenRepoParam(repaymentID)
		repayParam.CreatedAt = lsvc.timeMaker.Now()
		repayParam.UpdatedAt = lsvc.timeMaker.Now()
		repaymentRepoParam = append(repaymentRepoParam, repayParam)
	}

	err = lsvc.loanRepo.InsertRepayment(ctx, dbTx, confirmedLoanID, repaymentRepoParam)
	if err != nil {
		return "", fmt.Errorf("insert repayment %w", err)
	}

	if err := dbTx.Commit(); err != nil {
		return "", fmt.Errorf("commit %w", err)
	}

	return "PENDING", nil
}

func (lsvc *LoanService) ViewLoan(ctx context.Context, param model.ViewLoanRequest) ([]model.Loan, error) {
	res, err := lsvc.loanRepo.ViewLoan(ctx, param.UserID)
	if err != nil {
		return []model.Loan{}, err
	}

	// construct response
	var response []model.Loan
	for _, oneLoan := range res {
		loanData := model.Loan{
			Amount:      oneLoan.Amount,
			Currency:    oneLoan.Currency,
			Term:        oneLoan.Term,
			Status:      oneLoan.Status.String,
			RequestedAt: oneLoan.RequestedAt,
		}

		var repaymentData []model.LoanRepayment
		for _, oneRepayment := range oneLoan.Repayment {
			repaymentData = append(repaymentData, model.LoanRepayment{
				Amount:     oneRepayment.Amount,
				Schedule:   oneRepayment.Schedule,
				PaidAmount: oneRepayment.PaidAmount.String,
				Status:     oneRepayment.Status.String,
			})
		}

		loanData.Repayment = repaymentData
		response = append(response, loanData)
	}

	return response, nil
}

func (lsvc *LoanService) AdminView(ctx context.Context) ([]model.AdminLoan, error) {
	res, err := lsvc.loanRepo.AdminViewLoan(ctx)
	if err != nil {
		return []model.AdminLoan{}, err
	}

	// construct response
	var response []model.AdminLoan
	for _, oneLoan := range res {
		loanData := model.AdminLoan{
			Loan: model.Loan{
				Amount:      oneLoan.Amount,
				Currency:    oneLoan.Currency,
				Term:        oneLoan.Term,
				Status:      oneLoan.Status.String,
				RequestedAt: oneLoan.RequestedAt,
			},
			ID:     oneLoan.ID,
			UserID: oneLoan.UserID,
		}

		var repaymentData []model.LoanRepayment
		for _, oneRepayment := range oneLoan.Repayment {
			repaymentData = append(repaymentData, model.LoanRepayment{
				Amount:     oneRepayment.Amount,
				Schedule:   oneRepayment.Schedule,
				PaidAmount: oneRepayment.PaidAmount.String,
				Status:     oneRepayment.Status.String,
			})
		}

		loanData.Repayment = repaymentData
		response = append(response, loanData)
	}

	return response, nil
}

func (lsvc *LoanService) AdminApproval(ctx context.Context, param model.AdminApprovalRequest) (string, error) {
	dbTx, err := lsvc.loanRepo.BeginTx(ctx)
	if err != nil {
		return "", fmt.Errorf("begin %w", err)
	}

	defer func() {
		errRollback := dbTx.Rollback()
		if errRollback != nil && errRollback != sql.ErrTxDone {
			lib.LogData(ctx, errRollback)
		}
	}()

	_, err = lsvc.loanRepo.CheckLoanByID(ctx, dbTx, param.LoanID, "PENDING")
	if err != nil {
		return "", fmt.Errorf("check loan %w", err)
	}

	trailUUID, err := lsvc.uuidMaker.UUIDV1()
	if err != nil {
		return "", fmt.Errorf("uuid gen %w", err)
	}

	approvalTime := lsvc.timeMaker.Now()

	res, err := lsvc.loanRepo.AdminApproval(ctx, dbTx, model.AdminApprovalParam{
		AdminTrailID: trailUUID,
		AdminUserID:  param.AdminUserID,
		LoanID:       param.LoanID,
		ApprovedAt:   approvalTime,
		CreatedAt:    approvalTime,
	})
	if err != nil {
		return "", fmt.Errorf("admin approval %w", err)
	}

	err = lsvc.loanRepo.ApproveALoan(ctx, dbTx, param.LoanID)
	if err != nil {
		return "", fmt.Errorf("update loan %w", err)
	}

	if err := dbTx.Commit(); err != nil {
		return "", fmt.Errorf("commit %w", err)
	}

	return res, nil
}

func (lsvc *LoanService) PayRepayment(ctx context.Context, param model.PayRepaymentParam) (string, error) {
	dbTx, err := lsvc.loanRepo.BeginTx(ctx)
	if err != nil {
		return "", fmt.Errorf("begin %w", err)
	}

	defer func() {
		errRollback := dbTx.Rollback()
		if errRollback != nil && errRollback != sql.ErrTxDone {
			lib.LogData(ctx, errRollback)
		}
	}()

	loanData, err := lsvc.loanRepo.GetLoanByID(ctx, dbTx, param.UserID, param.LoanID)
	if err != nil {
		return "", fmt.Errorf("loan data %w", err)
	}

	calcResult, err := loanData.RepaymentProgressCheck(ctx, param.Amount)
	if err != nil {
		return "", fmt.Errorf("calculating repayment %w", err)
	}

	err = lsvc.loanRepo.PayRepayment(ctx, dbTx, model.PayRepaymentRepoParam{
		LoanID:      param.LoanID,
		RepaymentID: calcResult.RepaymentID,
		Amount:      param.Amount,
		Status:      calcResult.CurrentRepaymentStatus,
	})
	if err != nil {
		return "", fmt.Errorf("calculating repayment %w", err)
	}

	var finalVerdict = "PAID"
	if calcResult.NextLoanStatus == "PAID" {
		err = lsvc.loanRepo.FinishedALoan(ctx, dbTx, param.LoanID)
		if err != nil {
			return "", fmt.Errorf("calculating repayment %w", err)
		}

		finalVerdict = "FULLY PAID"
	}

	if err := dbTx.Commit(); err != nil {
		return "", fmt.Errorf("commit %w", err)
	}

	return finalVerdict, nil
}
