package lib

import (
	"context"
	"log"
	"time"

	"github.com/google/uuid"
)

func LogData(ctx context.Context, data interface{}) {
	log.Printf("%v:%v", ctx.Value("log"), data)
}

type UUIDV1Maker struct{}

func NewUUIDV1Maker() *UUIDV1Maker {
	return &UUIDV1Maker{}
}

func (uuidv1 *UUIDV1Maker) UUIDV1() (string, error) {
	newUUID, err := uuid.NewUUID()
	return newUUID.String(), err
}

type TimeMaker struct{}

func NewTimeMaker() *TimeMaker {
	return &TimeMaker{}
}

func (tm *TimeMaker) Now() string {
	return time.Now().Format("2006-01-02 15:05:06Z07:00")
}
