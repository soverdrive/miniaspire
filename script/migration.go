package main

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"os"

	_ "github.com/glebarez/go-sqlite"
)

func main() {
	fmt.Fprintf(os.Stdout, "connecting to database %s...\n", "mini_aspire.db")
	// connect
	db, err := sql.Open("sqlite", "./mini_aspire.db")
	if err != nil {
		fmt.Fprintf(os.Stderr, "opening sqlite %v\n", err)
		os.Exit(1)
	}

	fmt.Fprintf(os.Stdout, "reading init migration file at %s...\n", "./db/init.sql")
	// open migration for init tables
	initTables, err := ioutil.ReadFile("./db/init.sql")
	if err != nil {
		fmt.Fprintf(os.Stderr, "opening init tables file %v\n", err)
		os.Exit(1)
	}

	fmt.Fprintf(os.Stdout, "initializing tables...\n")
	_, err = db.Exec(string(initTables))
	if err != nil {
		fmt.Fprintf(os.Stderr, "creating tables %v\n", err)
		os.Exit(1)
	}

	fmt.Fprintf(os.Stdout, "reading seed migration file at %s...\n", "./db/seed.sql")
	// open migration for init tables
	seedTable, err := ioutil.ReadFile("./db/seed.sql")
	if err != nil {
		fmt.Fprintf(os.Stderr, "opening seed tables file %v\n", err)
		os.Exit(1)
	}

	fmt.Fprintf(os.Stdout, "seeding...\n")
	_, err = db.Exec(string(seedTable))
	if err != nil {
		fmt.Fprintf(os.Stderr, "seed tables %v\n", err)
		os.Exit(1)
	}

	fmt.Fprintf(os.Stdout, "done\n")
}
